const speed = 500

$(function () {
  $('img.logo').click(function () {
    $('body, html').animate({ scrollTop: $('#home').offset().top - 50 }, speed)
  })

  $('body, nav td, nav a').on('click', function (e) {
    var mobNavContainer = $('#mobNavContainer')
    var startMobNav = $('#startMobNav')

    if (mobNavContainer.css('top') == '50px') {
      mobNavContainer.stop().animate({ 'top': '-400' }, speed)
      startMobNav.removeClass('rotate')
    } else if ($(e.target).is('#startMobNav')) {
      mobNavContainer.stop().animate({ 'top': '50' }, speed)
      startMobNav.addClass('rotate')
    }
    if (!$(e.target).is('#startMobNav')) {
      if ($(e.target).stop().is('#toHome')) {
        $('body, html').stop().animate({ scrollTop: $('#home').offset().top - 50 }, speed)
      } else if ($(e.target).stop().is('#toContact')) {
        $('body, html').stop().animate({ scrollTop: $('#contact').offset().top - 50 }, speed)
      } else if ($(e.target).stop().is('#toNews')) {
        $('body, html').stop().animate({ scrollTop: $('#news').offset().top - 50 }, speed)
      } else if ($(e.target).stop().is('#toProducts')) {
        $('body, html').stop().animate({ scrollTop: $('#products').offset().top - 50 }, speed)
      } else if ($(e.target).stop().is('#toCustomers')) {
        $('body, html').stop().animate({ scrollTop: $('#customers').offset().top - 50 }, speed)
      } else if ($(e.target).stop().is('#toSuppliers')) {
        $('body, html').stop().animate({ scrollTop: $('#suppliers').offset().top - 50 }, speed)
      }
    }
  })

  $(window).scroll(function () {
    var curPos = $(window).scrollTop() + 50

    var contact = $('#contact')
    var news = $('#news')
    var products = $('#products')
    var customers = $('#customers')
    var suppliers = $('#suppliers')

    if (curPos >= $('nav.fixed').offset().top && curPos <= contact.offset().top) {
      $('a').removeClass('selected')
    }
    if (curPos >= contact.offset().top && curPos <= news.offset().top) {
      $('a').not('#toContact').removeClass('selected')
      $('#toContact').addClass('selected')
    }
    if (curPos >= news.offset().top && curPos <= products.offset().top) {
      $('a').not('#toNews').removeClass('selected')
      $('#toNews').addClass('selected')
    }
    if (curPos >= products.offset().top && curPos <= customers.offset().top) {
      $('a').not('#toProducts').removeClass('selected')
      $('#toProducts').addClass('selected')
    }
    if (curPos >= customers.offset().top && curPos <= suppliers.offset().top) {
      $('a').not('#toCustomers').removeClass('selected')
      $('#toCustomers').addClass('selected')
    }
    if (curPos >= suppliers.offset().top) {
      $('a').not('#toSuppliers').removeClass('selected')
      $('#toSuppliers').addClass('selected')
    }
  })
})

function showImprint () {
  document.querySelector('#main').style.display = 'none'
  document.querySelector('#bigHeader').style.display = 'none'
  document.querySelector('#imprint').style.display = 'block'
  document.querySelector('#smallHeader').style.display = 'block'
}

function showPrivacy () {
  document.querySelector('#main').style.display = 'none'
  document.querySelector('#bigHeader').style.display = 'none'
  document.querySelector('#privacy').style.display = 'block'
  document.querySelector('#smallHeader').style.display = 'block'
}
